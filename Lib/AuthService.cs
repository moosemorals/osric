﻿using Microsoft.EntityFrameworkCore;

using osric.uk.Lib.Storage;
using osric.uk.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace osric.uk.Lib
{
    public class AuthService : IPeopleStorage
    {
        private static readonly PasswordHashOptions _passwordHashOptions = new();
        private readonly Context _db;
        private readonly PasswordHasher _hasher = new(_passwordHashOptions);

        public AuthService(Context db)
        {
            _db = db;
        }

        public Task<Result<Person>> CreatePerson(string name, string password, params Role[] roles)
        {
            return _db.AtomicAsync(async () => { 
            
                Person? user = await _db.People
                    .SingleOrDefaultAsync(p => p.Name == name);
                if (user != null)
                {
                    return new Result<Person>("User already exists");
                }

                user = new Person(default, name, _hasher.Create(password), DateTimeOffset.MinValue);
                foreach (Role role in roles) {
                    user.Roles.Add(role);
                }

                _db.People.Add(user);

                return new Result<Person>(user);
            });
        }

        public Task<Result<Role>> CreateRole(string name)
        {
            return _db.AtomicAsync(async () =>
            { 
                Role? role = await _db.Roles.SingleOrDefaultAsync(r => r.Name == name);
                if (role == null)
                {
                    role = new Role(default, name);
                    _db.Roles.Add(role); 
                }
                return new Result<Role>(role);
            }); 
        }

        public Task<Result<Role>> GetRole(string name)
        {
            return _db.AtomicAsync(async () =>
            { 
                Role? role = await _db.Roles.FirstOrDefaultAsync(r => r.Name == name);
                if (role == null)
                {
                    return new Result<Role>("Role not found");
                }
                Role result = role;
                return new Result<Role>(result);
            });

        }

        public Task<Result<Person>> Login(string name, string password)
        {
            return _db.AtomicAsync(async () => { 
            
                Person? user = await _db.People 
                    .Include(p => p.Roles)
                    .SingleOrDefaultAsync(p => p.Name == name);

                if (user == null)
                {
                    return new Result<Person>("Username or password invalid");
                }

                (bool valid, bool needsUpdate) = _hasher.Verify(user.Password, password);
                if (!valid)
                {
                    return new Result<Person>("Username or password invalid"); 
                }

                if (needsUpdate)
                {
                    user.Password = _hasher.Create(password);
                }
                user.LastLogin = DateTimeOffset.Now;


                return new Result<Person>(user); 
            });
        }

    }
}
