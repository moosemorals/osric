
namespace osric.uk.Lib
{
    public static class Routes
    {
        public const string TopIndex = "routes.osric.uk.topindex";
        public const string SignedOut = "routes.osric.uk.signedout";
        public const string Login = "routes.osric.uk.Login";
        public const string Logout = "routes.osric.uk.Logout";

    }

}
