

using System;
using System.Linq;
using System.Security.Cryptography;

namespace osric.uk.Lib
{

    public class PasswordHashOptions
    {
        public int SaltSize { get; set; } = 16;
        public int KeySize { get; set; } = 32;
        public int Rounds { get; set; } = 10000;
        public HashAlgorithmName HashType { get; set; } = HashAlgorithmName.SHA256;
    }

    // Via https://medium.com/dealeron-dev/storing-passwords-in-net-core-3de29a3da4d2
    public sealed class PasswordHasher
    {
        private const string _seperator = ".";
        private readonly PasswordHashOptions _options;

        public PasswordHasher(PasswordHashOptions options)
        {
            _options = options;
        }

        public string Create(string password)
        {
            using (var alg = new Rfc2898DeriveBytes(
                password,
                _options.SaltSize,
                _options.Rounds,
                _options.HashType
            ))
            {
                string key = Convert.ToBase64String(alg.GetBytes(_options.KeySize));
                string salt = Convert.ToBase64String(alg.Salt);

                return string.Join(_seperator,
                    _options.HashType,
                    _options.Rounds,
                    salt,
                    key);
            }
        }

        public (bool Valid, bool NeedsUpgrade) Verify(string hash, string password)
        {
            bool NeedsUpgrade = false;
            string[] parts = hash.Split(_seperator, 4);

            if (parts.Length != 4)
            {
                throw new ArgumentException("Unexpected password hash format");
            }

            HashAlgorithmName hashType= GetHashAlgorithmName(parts[0]);

            if (!int.TryParse(parts[1], out int rounds))
            {
                throw new ArgumentException("Can't parse rounds");
            }

            byte[] salt = Convert.FromBase64String(parts[2]);
            byte[] key = Convert.FromBase64String(parts[3]);

            if (rounds != _options.Rounds || hashType != _options.HashType)
            {
                NeedsUpgrade = true;
            }

            using (var alg = new Rfc2898DeriveBytes(
                password,
                salt,
                rounds,
                hashType
            )) {
                byte[] keyToCheck = alg.GetBytes(_options.KeySize);
                bool Verify= keyToCheck.SequenceEqual(key);

                return (Verify, NeedsUpgrade);
            }
        }

        private static HashAlgorithmName GetHashAlgorithmName(string name) {
            switch (name) {
                case "SHA256":
                    return HashAlgorithmName.SHA256;
                default:
                    throw new ArgumentException("Unknown hash algorithm");
            }
        }
    }
}