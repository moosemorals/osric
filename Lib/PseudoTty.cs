﻿using Microsoft.Win32.SafeHandles;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace osric.uk.Lib
{
    public sealed class PseudoTty : IDisposable 
    {
        private const int BuffSize = 1024 * 4;
        private Thread? _readThread;
        private readonly Process _proc;
        private bool disposedValue;

        public event EventHandler<string>? TextReceived;

        public PseudoTty() {
            ProcessStartInfo psi = new ProcessStartInfo {
                CreateNoWindow = true,
                FileName = "/home/osric/Projects/ptswrapper/main",
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
            };

            _proc = new Process() {
                StartInfo = psi,                
            };
        }

        public void Start() {

            _proc.Start();

            _readThread = new Thread(() => {
                StreamReader r = _proc.StandardOutput;

                char[] buffer = new char[BuffSize];

                int read;
                while ((read = r.Read(buffer, 0, BuffSize))> 0) {
                    OnTextReceived(new String(buffer, 0, read));
                }
            });

            _readThread.IsBackground = true;
            _readThread.Start();
        }

        public void Write(string text) {
            _proc.StandardInput.Write(text);
            _proc.StandardInput.Flush();
        }

        private void OnTextReceived(string text) {
            EventHandler<string>? handler = TextReceived;
            handler?.Invoke(this, text);
        }

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _proc.WaitForExit();
                    _proc.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
