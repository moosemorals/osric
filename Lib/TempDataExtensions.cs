using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace osric.uk.Lib
{

    public static class TempDataExtensions
    {

        public static void AddMessage(this ITempDataDictionary tempData, string message)
        {
            List<string> messages;
            if (tempData.ContainsKey(Names.Messages))
            {
                messages = new List<string>((string[])tempData[Names.Messages]);

                tempData.Remove(Names.Messages);
            }
            else
            {
                messages = new();
            }
            messages.Add(message);
            tempData.Add(Names.Messages, messages.ToArray());
        }

        public static bool HasMessages(this ITempDataDictionary tempData)
        {
            return tempData.ContainsKey(Names.Messages);
        }

        public static IEnumerable<string> GetMessages(this ITempDataDictionary tempData)
        {
            if (tempData.ContainsKey(Names.Messages))
            {
                return (string[])tempData[Names.Messages];
            }
            return Array.Empty<string>();
        }

    }

}