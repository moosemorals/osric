
namespace osric.uk.Lib
{
    public static class Names {

        public static class Roles {
            public const string User = "uk.osric.roles.user";
        }

        public static class Routes {
            public const string TopIndex = "uk.osric.routes.index";
            public const string Terminal = "uk.osric.routes.terminal";
            public const string Login = "uk.osric.routes.login";
            public const string Logout = "uk.osric.routes.logout";
            public const string AuthFail = "uk.osric.routes.authfail";
        }

        public const string AuthScheme = "uk.osric.auth.cookies";
        public const string Messages = "uk.osric.auth.messages";
    }
}