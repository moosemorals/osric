﻿using osric.uk.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace osric.uk.Lib.Storage
{
    public interface IPeopleStorage
    {
        Task<Result<Person>> CreatePerson(string name, string password, params Role[] roles);

        Task<Result<Person>> Login(string name, string password);

        Task<Result<Role>> CreateRole(string name);

        Task<Result<Role>> GetRole(string name);
    }
}
