

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using osric.uk.Models;

using Serilog;

using System;
using System.Reflection;
using System.Threading.Tasks;

namespace osric.uk.Lib.Storage
{
    public class Context : DbContext
    {
        private readonly ILogger _log = Log.ForContext<Context>();

        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public DbSet<Note> Notes => Set<Note>();
        public DbSet<Person> People => Set<Person>();
        public DbSet<Role> Roles => Set<Role>();

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<Person>()
                .HasMany(p => p.Roles)
                .WithMany(r => r.People);
        }

        public async Task<TResult> AtomicAsync<TResult>(Func<Task<TResult>> job, bool saveChanges = true) where TResult : Result
        {
            using IDbContextTransaction t = await Database.BeginTransactionAsync();
            try
            {
                TResult result = await job();
                if (saveChanges && result.IsSuccess)
                {
                    await SaveChangesAsync();
                    t.Commit();
                }
                return result;
            }
            catch (Exception ex)
            {
                t.Rollback();

                _log.Error(ex, "Unexpected error: {errorMessage}", ex.Message);
                ConstructorInfo? ci = typeof(TResult).GetConstructor(new Type[] { typeof(Exception) });
                if (ci != null)
                {
                    return (TResult)ci.Invoke(new object[] { ex });
                }
                throw new Exception("Can't create result for exception", ex);
            }
        }





    }
}
