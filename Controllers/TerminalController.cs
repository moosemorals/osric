
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using osric.uk.Lib;

namespace osric.uk.Controllers
{


    [Authorize]

    [Route("/terminal")]
    public class TerminalController : Controller
    {
        private const int BuffSize = 1024 * 4;

        private static PseudoTty? _ptty;

        [HttpGet("", Name = Names.Routes.Terminal)]
        public ActionResult Terminal() => View();

        [HttpPost("stdin")]
        public async Task<ActionResult> StdIn()
        {
            string input = "";
            byte[] buffer = new byte[BuffSize];
            int read;
            while ((read = await Request.Body.ReadAsync(buffer, 0, BuffSize)) != 0)
            {
                input += Encoding.UTF8.GetString(buffer, 0, read);

            }
            if (_ptty != null)
            {
                _ptty.Write(input);
            }
            return Accepted();
        }

        [HttpGet("stdout")]
        public async Task StdOutAsync()
        {
            CancellationToken ct = HttpContext.RequestAborted;

            using (_ptty = new PseudoTty())
            {
                async void _handler(object? source, string text)
                {
                    byte[] bytes = Encoding.UTF8.GetBytes(text);
                    await Response.Body.WriteAsync(bytes, 0, bytes.Length, ct);
                    await Response.Body.FlushAsync(ct);
                }

                _ptty.TextReceived += _handler;

                _ptty.Start();

                Response.Headers.Add("Cache-control", "no-store,max-age=0");
                Response.ContentType = "text/plain;charset=utf8";
                await Response.StartAsync(ct);

                await Task.Delay(-1, ct);
            }
        }
    }
}