

using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using osric.uk.Lib;
using osric.uk.Models;
using osric.uk.ViewModels;
using Serilog;

namespace osric.uk.Controllers
{

    [Route("Account")]
    public class AccountController : Controller
    {
        private readonly ILogger _log = Log.ForContext<AccountController>();

        [HttpGet("Login", Name = Routes.Login)]
        public async Task<IActionResult> LoginAsync(LoginVM model)
        {
            // Clear current cookie
            await HttpContext.SignOutAsync(Names.AuthScheme);
            return View("Login", model);
        }

        [HttpPost("Login")]
        public async Task<IActionResult> LoginPostAsync(LoginVM model) {

            if (model.Username == null) {
                ModelState.AddModelError(nameof(model.Username), "Missing username");
            }

            if (model.Password == null)
            {
                ModelState.AddModelError(nameof(model.Password), "Missing password");
            }

            if (!ModelState.IsValid || model.Username == null || model.Password == null)
            {
                return View("Login", model);
            }

            User? user = DoAuth(model.Username, model.Password);

            if (user == null)
            {
                _log.Warning("Login {loginStatus} for '{loginUsername}'", "failed", model.Username);
                ModelState.AddModelError(string.Empty, "Failed login");
                return View("Login", model);
            }
             _log.Information("Login {loginStatus} for '{loginUsername}'", "success", model.Username);


            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.Role, "User")
            };

            ClaimsIdentity identity = new ClaimsIdentity(claims, Names.AuthScheme);

            AuthenticationProperties authProps = new AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                IssuedUtc = DateTimeOffset.Now,
                RedirectUri = model.ReturnUrl ?? Url.RouteUrl(Routes.TopIndex),
            };

            await HttpContext.SignInAsync(
                Names.AuthScheme,
                new ClaimsPrincipal(identity),
                authProps
            );

            if (Url.IsLocalUrl(model.ReturnUrl))
            {
                return LocalRedirect(model.ReturnUrl);
            }
            return RedirectToRoute(Routes.TopIndex);
        }

        [HttpGet("signedOut", Name = Routes.SignedOut)]
        public IActionResult SignedOut() => View();

        [HttpPost("logout", Name = Routes.Logout)]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(Names.AuthScheme);
            return RedirectToRoute(Routes.SignedOut);
        }


        private User? DoAuth(string username, string password)
        {
            // TODO: Better auth
            return new User(username, password);
        }
    }

}
