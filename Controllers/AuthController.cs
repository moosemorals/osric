

using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using osric.uk.Lib;
using osric.uk.Lib.Storage;
using osric.uk.Models;
using osric.uk.ViewModels;
using Serilog;

namespace osric.uk.Controllers
{

    public class AuthController : Controller
    {

        private readonly ILogger _log = Log.ForContext<AuthController>();

        private readonly IPeopleStorage _people;

        public AuthController(IPeopleStorage people)
        {
            _people = people;
        }

        [HttpGet("/login", Name = Names.Routes.Login)]
        public async Task<IActionResult> GetLoginAsync([Bind(LoginVM.BindNames)] LoginVM model)
        {
            await HttpContext.SignOutAsync(Names.AuthScheme);
            return View("Login", model);
        }

        [HttpPost("/login")]
        public async Task<IActionResult> PostLoginAsync([Bind(LoginVM.BindNames)] LoginVM model)
        {
            if (model.Username == null || model.Password == null || !ModelState.IsValid)
            {
                return View("Login", model);
            }

            Result<Person> login = await _people.Login(model.Username, model.Password);
            if (!login.IsSuccess)
            {
                _log.Warning("Login {loginState} for user {userName}", "failed", model.Username);
                TempData.AddMessage("Login failed");
                return View("Login", model);
            }

            _log.Information("Login {loginState} for user {userName}", "succeeded", model.Username);
            Person user = login.Payload;

            List<Claim> claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.Name),
            };

            claims.AddRange(user.Roles.Select(r =>
                new Claim(ClaimTypes.Role, r.Name)
            ));

            ClaimsIdentity identity = new ClaimsIdentity(claims, Names.AuthScheme);

            string redirectUrl = CheckRedirectUrl(model.ReturnUrl);

            AuthenticationProperties authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                IssuedUtc = System.DateTimeOffset.Now,
                RedirectUri = redirectUrl,
            };

            await HttpContext.SignInAsync(
                Names.AuthScheme,
                new ClaimsPrincipal(identity),
                authProperties
            );


            TempData.AddMessage($"Welcome back {user.Name}");

            return LocalRedirect(redirectUrl);
        }

        [HttpPost("/logout", Name = Names.Routes.Logout)]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(Names.AuthScheme);

            TempData.AddMessage("Goodbye!");
            return RedirectToRoute(Names.Routes.TopIndex);
        }

        private string CheckRedirectUrl(string? url)
        {
            if (string.IsNullOrEmpty(url) || !Url.IsLocalUrl(url))
            {
                return Url.RouteUrl(Names.Routes.TopIndex);
            }
            return url;
        }
    }
}