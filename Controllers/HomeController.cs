
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using osric.uk.Lib;
using osric.uk.Lib.Storage;
using osric.uk.ViewModels;

namespace osric.uk.Controllers
{
    public class HomeController : Controller
    {

        private readonly Context _db;

        public HomeController(Context db)
        {
            _db = db;
        }

        [HttpGet("", Name =  Names.Routes.TopIndex)]
        public async Task<IActionResult> IndexAsync()
        {
            return View(new IndexVM(await _db.Notes.ToListAsync()));
        }

    }
}
