

using System.Collections.Generic;
using osric.uk.Models;

namespace osric.uk.ViewModels
{
    public class IndexVM
    {

        public IEnumerable<Note> Notes { get; init; }

        public IndexVM(IEnumerable<Note> notes)
        {
            Notes = notes;
        }
    }

}
