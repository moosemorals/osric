
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace osric.uk.ViewModels {

    public class LoginVM {
        [Required]
        public string? Username {get;set;}

        [Required]
        [DataType(DataType.Password)]
        public string? Password {get;set;}

        [HiddenInput]
        public string? ReturnUrl {get;set;}

        public const string BindNames = nameof(Username)
            + "," + nameof(Password)
            + "," + nameof(ReturnUrl);
    }
}