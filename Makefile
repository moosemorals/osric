
.PHONY: build clean deploy

build: clean
	mkdir target
	dotnet publish --nologo --configuration release --output target --no-self-contained --runtime linux-arm64

deploy: build
	rsync --archive --delete target/ osricuk@webhead:~/deploy

clean:
	rm -rf target
