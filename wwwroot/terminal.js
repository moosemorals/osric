
async function connect() {
    const response = await fetch("/terminal/stdout")
    const utf8 = new TextDecoder();
    const reader = response.body.getReader();

    return {

        _buff: "",

        [Symbol.asyncIterator]() {
            return this;
        },

        _next() {
            const c = this._buff[0];
            this._buff = this._buff.substring(1);
            return { done: false, value: c };
        },

        async next() {
            if (this._buff.length > 0) {
                return this._next();
            }

            const { done, value } = await reader.read();
            if (done) {
                return { done: true };
            }
            this._buff = utf8.decode(value);
            return this._next();
        },

        pushback(c) {
            this._buff += c + this._buff;
        }
    }
}

function CSI(cmd) { return `\x1b[${cmd}`; }

const keymap = {
    Enter: "\n",
    Backspace: "\x7f",
    ArrowLeft: CSI("D"),
    ArrowRight: CSI("C")
};

function input() {
    document.addEventListener("keydown", e => {
        e.preventDefault();
        let key;
        if (e.key.length == 1) {
            key = e.key;
        } else if (e.key in keymap) {
            key = keymap[e.key]
        }

        if (key != null) {
            fetch("/terminal/stdin", {
                method: "POST",
                headers: {
                    "Content-Type": "text/plain;charset=utf8"
                },
                body: key
            });
        } else {
            console.log(e.key);
        }
    });
}

function getScreen() {
    const s = document.getElementById("terminal");
    return {
        display: s,
        cols: parseInt(getComputedStyle(s).getPropertyValue("--width")),
        rows: parseInt(getComputedStyle(s).getPropertyValue("--height"))
    };
}

const screen = getScreen();

function clearScreen() {
    while (screen.display.firstChild) {
        screen.display.removeChild(screen.display.firstChild);
    }
    cursor.row = 0;
    cursor.col = 0;
}

/**
 * @returns {Array<HTMLDivElement,Text>}
 */
function newline() {
    const line = document.createElement("div");
    const text = document.createTextNode("");
    line.appendChild(text);
    cursor.col = 0;
    return [line, text];
}

const cursor = { col: 0, row: 0, display: null };

function moveCursor(col, row) {
    const r = screen.display.children[row];
    // Walk through the row until we find the col-th character
    // then split the text node and move cursor.display

    let index = 0;
    for (const child of r.childNodes) {
        let x = (child.nodeType == Node.TEXT_NODE) ? child : child.firstChild;
        if (x == null) {
            continue;
        }
        if (index + x.length < col) {
            index += x.length;
            continue;
        }

        const next = x.splitText(col - index);
        r.insertBefore(cursor.display, next);
        break;
    }

    cursor.row = row;
    cursor.col = col;
}

async function output() {
    cursor.display = document.createElement("span");
    cursor.display.classList.add("cursor");

    clearScreen();
    let [line, text] = newline();
    line.appendChild(cursor.display);
    const output = screen.display;
    output.appendChild(line);

    const stdout = await connect();
    for await (const c of stdout) {
        switch (c) {
            case '\u0008': {
                // Backspace
                text.deleteData(text.length - 1, 1);
                moveCursor(cursor.col - 1, cursor.row);
                break;
            }
            case '\u0009': {
                // Tab
                for (let s = 8 - (cursor.col % 8); s > 0; s -= 1) {
                    text.appendData(' ');
                    moveCursor(cursor.col + 1, cursor.row);
                }
                break;
            }
            case '\u000A': {
                // Newline
                [line, text] = newline();
                line.appendChild(cursor.display);
                output.appendChild(line);
                moveCursor(cursor.col, cursor.row + 1);

                // Scroll at the end of the sceen
                if (output.children.length > screen.rows) {
                    output.removeChild(output.firstChild);
                    moveCursor(cursor.col, cursor.row - 1);
                }
                break;
            }
            case '\u000D':
                // Carrage return
                moveCursor(0, cursor.row);
                break;

            case '\u001B': {
                let n = (await stdout.next()).value;
                if (n != '[') {
                    stdout.pushback(n);
                    break;
                }

                const params = [];
                while (true) {
                    let param = "";
                    n = (await stdout.next()).value;
                    if (n >= '0' && n <= '9') {
                        param += n;
                    } else {
                        if (param.length > 0) {
                            params.push(parseInt(param));
                        }
                        if (n != ';') {
                            stdout.pushback(n);
                            break;
                        }
                    }
                }

                const cmd = (await stdout.next()).value;
                switch (cmd) {
                    case 'C':
                        moveCursor(cursor.col + (params[0] ?? 1), cursor.row);
                        break;
                    case 'D':
                        moveCursor(cursor.col - (params[0] ?? 1), cursor.row);
                        break;
                    case 'm':
                        console.log("Graphics ", params);
                        break;
                }

                break;
            }
            default: {
                // everything else
                let x = c;
                // Control character
                if (c.charCodeAt(0) < 0x20) {
                    x = '^' + String.fromCharCode(c.charCodeAt(0) + 0x40);
                }

                // Either append to end
                if (cursor.col == text.nodeValue.length) {
                    text.appendData(x);
                } else {
                    text.replaceData(cursor.col, 1, x);
                }
                moveCursor(cursor.col + x.length, cursor.row);
                break;
            }
        }
    }
}

async function init() {
    input();
    await output();
}

document.addEventListener("DOMContentLoaded", init);
