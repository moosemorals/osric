

function pad2(num) {
    if (num < 10) {
        return "0" + num;
    }
    return "" + num;
}

function tick(clock) {
    requestAnimationFrame(() => tick(clock));
    const now = new Date();
    const time = pad2(now.getHours()) + ":" + pad2(now.getMinutes());

    clock.textContent = time;
}

function init() {
    const clock = document.getElementById("clock");
    if (clock != null) {
        tick(clock);
    }
}


document.addEventListener("DOMContentLoaded", init);
