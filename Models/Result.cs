﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace osric.uk.Models
{
    public class Result
    {
        public Result()
        {
            IsSuccess = true;
            Exception = null;
            Problems = Array.Empty<string>();
        }

        public Result(Exception ex)
        {
            IsSuccess = false;
            Exception = ex;
            Problems = new string[] { ex.Message };
        }

        public Result(params string[] problems)
        {
            IsSuccess = false;
            Exception = null;
            Problems = problems;
        }

        public virtual bool IsSuccess { get;  }

        public Exception? Exception { get; }

        public IEnumerable<string> Problems { get; }
    }

    public class Result<T> : Result
    {
        private readonly T? _payload;

        public Result(T payload) : base()
        {
            if (payload == null)
            {
                throw new ArgumentNullException(nameof(payload), "Use non-generic Result for null payload");
            }
             _payload = payload;
        }

        public Result(Exception ex) : base(ex) { }
        public Result(params string[] problems) : base(problems) { }

        public T Payload => _payload ?? throw new NullReferenceException("Payload unexpectedly null");
    }


}
