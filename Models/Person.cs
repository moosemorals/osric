
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace osric.uk.Models {

    public class Person {
        public Person(
            long Id, 
            string Name, 
            string Password,
            DateTimeOffset LastLogin)
        {
            this.Id = Id;
            this.Name = Name;
            this.Password = Password;
            this.LastLogin = LastLogin;
            Roles = new List<Role>();
        }

        [Key]
        public long Id {get;set;}

        [Required]
        [MaxLength(255)]
        public string Name {get;set;}

        [Required]
        public string Password {get;set;}

        [Required]
        public DateTimeOffset LastLogin {get;set;}

        public ICollection<Role> Roles {get;set;} 
    }

    public class Role {
        public Role(long Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
            People = new List<Person>();
        }

        [Key]
        public long Id {get;set;}

        [Required]
        public string Name {get;set;}

        public ICollection<Person> People {get;set;}
    }

}