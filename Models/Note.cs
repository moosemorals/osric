
using System;
using System.ComponentModel.DataAnnotations;

namespace osric.uk.Models
{

    public record Note(
        [Required] long Id,
        [Required] DateTimeOffset CreatedAt,
        [Required] string Text,
        long? ReplyTo);

}
