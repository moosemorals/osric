//  SPDX-License-Identifier: ISC

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using osric.uk.Lib;
using osric.uk.Lib.Storage;
using osric.uk.Models;

using Serilog;
using Serilog.Events;

namespace osric.uk
{
    public class Program
    {
        public static async Task<int> Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            try
            {
                Log.Information("Starting web host");
                IHost? host = CreateHostBuilder(args).Build();

                await SetupDatabaseAsync(host);

                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Web host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
		.UseSystemd()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private static async Task SetupDatabaseAsync(IHost host)
        {
            using var scope = host.Services.CreateScope();
            Context db = scope.ServiceProvider.GetRequiredService<Context>();
            db.Database.Migrate();

            if (!db.People.Any())
            {
                IPeopleStorage ps = scope.ServiceProvider.GetRequiredService<IPeopleStorage>();

                Result<Role> userRole = await ps.CreateRole(Names.Roles.User);
                if (!userRole.IsSuccess)
                {
                    throw new Exception("Can't create role" );

                }

                string password = GenerateRandomPassword();
                Result<Person> result = await ps.CreatePerson("Osric", password, userRole.Payload);
                if (result.IsSuccess)
                {
                    Person who = result.Payload;
                    Log.Information("Created new user {userName} with password {password}", who.Name, password);
                }
            }
        }

        private static string GenerateRandomPassword()
        {
            byte[] noise = new byte[16];
            RNGCryptoServiceProvider rand = new();
            rand.GetBytes(noise);
            return Convert.ToHexString(noise);
        }
    }
}
